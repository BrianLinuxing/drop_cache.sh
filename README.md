# Drop_cache.sh

A quick and dirty lump of bash, not recommended for servers, but OK(ish) for Small Board Computers or older, memory restricted systems. Generally run via cron, every minute to so.

# The real world Why: 

Imagine you have a key machine, it runs Linux, and just does a job every day without needing to reboot for years. For years.

Yet every once in a while Chrome (or some other browser) kicks off to do some key function and exhausts the free memory (it's a small machine 1Gb or less). At that point, swapping kicks in and the machine grinds to a halt for 2-3 minutes while Linux sorts itself out. That in turn adversely impinges on business and causes other difficulties elsewhere.

You could fiddle with the swapiness parameters, but they don’t really cure the problem

So instead you decide to avoid swapping under Linux at almost any cost by regularly dropping the cache.

Yes, there is a performance hit. Not recommended on a server. Not recommended at all.

But on a small board computer or smallish interactive machine such a hit is typically less than a violent bout of swapping caused by no more free memory. 

Therefore, it is a balance between the negative hit that excessive swapping will cause or the impact of dropping the cache (i.e. used during file copies, etc.).

So if your machine is getting hit on memory resources and can’t be upgraded, then this code might help you.

Enjoy!
