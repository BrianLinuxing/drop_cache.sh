#!/bin/bash
#
# Program: drop_cache.sh
# Author(s): Brian Byrne.
# Date: 2008 (or before).
#
# Requires: Bash shell, runs from root, needs cron too.
#
# The Licence: Copy it freely, use it, enjoy it, but purely at your own risk.
# NB: Don't blame me if anything goes wrong!
#
# The What: Regularly free up memory on Linux systems. B.Byrne, 2008(ish)
#
# The Why: A quick and dirty method, not recommended for servers,
#          but OK(ish) for SBC or older, memory restricted systems.
#          Generally run via cron, every minute to so.
#
# The How:
#
# 1. Create the file drop_cache.sh in root
# 2. Make it executable with chmod u+x
# 3. Add this to root's cron entries to run every minute:
# * * * * * /root/drop_cache.sh > /root/drop_cache.log 2>&1
# 4. Verify by looking at kernel message buffer (dmesg), should see something like:
# "sh (6906): drop_caches: 3" and look at drop_cache.log in the root directory
#
# The Real Code:
#
free -m
sync
echo 3 > /proc/sys/vm/drop_caches
free -m
exit